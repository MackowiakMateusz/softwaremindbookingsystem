End points are in the swagger when the program is started

For easier checking of functionality without registering and logging in and pasting the token each time, the best
enter desk and location controls and crash:
[Authorize (Roles = UserRoles.Admin)]
[Authorize (Roles = UserRoles.Employee)]
with ctrl + f and replace all

All items except one unit test done

To get the role of the admin and use the endpoints for the admin, you need to register as an admin under the endpoint
register-admin
Same with employee

I did not give the required endpoints for employee employee role, because there are only 2 roles and it is important that the employee does not have access to the endpoint administrator

Authorization by JWT token
Authentication by the Microsoft.AspNetCore.Authorization library;


Mateusz Maćkowiak
==================================================================================================
Polish version
End pointy są w swaggerze, po uruchomieniu programu

Dla łatwiejszego sprawdzania funkcjonalnosci bez rejestracji i logowania i wklejania tokena za kazdym razem, najlepiej
wejsc w kontrollery desk i location i wywalic:
[Authorize(Roles = UserRoles.Admin)]
[Authorize(Roles = UserRoles.Employee)]
poprzez ctrl+f i replace all 

Wszystkie podpunkty poza jednym unit testem zrobione

Żeby dostać rolę admina i korzystac z endpointow dla admina trzeba sie zarejestrować jako admin pod endpointem
register-admin
To samo z employee

nie dałem dla endpointow dla employee roli employee wymaganej, bo są tylko 2 role i ważne żeby employee nie miał do admina end pointow dostepu

Authoryzacja przez JWT token
Authentykacja przez biblioteke Microsoft.AspNetCore.Authorization;


Mateusz Maćkowiak