﻿namespace SoftwareMindDeskBookingSystem.Models
{
    public class DeskRepository : IDeskRepository
    {
        private List<Desk> desks = new List<Desk>();
        private int _nextId = 1;

        public DeskRepository()
        {
            Add(new Desk { Name = "YellowDesk", Location = "1stFloor", BookedBy = "Bob", BookingDate = DateTime.UtcNow, BookingExpirationDate = DateTime.UtcNow.AddDays(1) });
            Add(new Desk { Name = "BlueDesk", Location = "1stFloor", BookedBy = "Jan", BookingDate = DateTime.UtcNow, BookingExpirationDate = DateTime.UtcNow.AddDays(1) });
            Add(new Desk { Name = "GreenDesk", Location = "2ndFloor", BookedBy = "Dzban",BookingDate= DateTime.UtcNow, BookingExpirationDate= DateTime.UtcNow.AddDays(1) });
            Add(new Desk { Name = "RecentlyNewDesk", Location = "2ndFloor", BookedBy = "", BookingDate = DateTime.UtcNow, BookingExpirationDate = DateTime.UtcNow.AddDays(1) });
        }
        public IEnumerable<Desk> GetAllAdmin()
        {
            return desks;
        }
        public IEnumerable<Desk> GetAll()
        {
            //List<Desk> employeeDesks = desks.ConvertAll<Desk>(x => { x.BookedBy= ""; return x; });
            return desks.ConvertAll<Desk>(x => { x.BookedBy= ""; return x; });
        }

        public Desk Get(int id)
        {
            Desk returndesk = desks.Find(p => p.Id == id);//tam gdzie tego nie ma tylko admin ma dostep w controllerze jest tag
            returndesk.Location = "";
            return returndesk;
        }

        public Desk Add(Desk item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("item");
            }
            item.Id = _nextId++;
            desks.Add(item);
            return item;
        }

        public void Remove(int id)
        {
            desks.RemoveAll(p => p.Id == id);
        }

        public bool Update(Desk item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("item");
            }
            int index = desks.FindIndex(p => p.Id == item.Id);
            if (index == -1)
            {
                return false;
            }
            desks.RemoveAt(index);
            desks.Add(item);
            return true;
        }
        public bool BookDeskForOneDay(int id, string username)
        {
            if (username == null || username == "")
            {
                throw new ArgumentNullException("Provided username is empty");
            }
            int index = desks.FindIndex(p => p.Id == id);
            if (index == -1)
            {
                return false;
            }
            Desk temporalDesk = desks[index];
            if (temporalDesk.BookedBy != "")
            {
                throw new ArgumentNullException("Desk is already booked by other user");
            }
            temporalDesk.BookedBy = username;
            //BookingDate = DateTime.UtcNow, BookingExpirationDate = DateTime.UtcNow.AddDays(1)
            temporalDesk.BookingDate = DateTime.UtcNow;
            temporalDesk.BookingExpirationDate = DateTime.UtcNow.AddDays(1);
            desks.RemoveAt(index);
            desks.Add(temporalDesk);
            return true;
        }
        public bool BookDeskForMultipleDaysButNotMoreThanWeek(int id, int days, string username)
        {
            if (username == null || username == "")
            {
                throw new ArgumentNullException("Provided username is empty");
            }
            int index = desks.FindIndex(p => p.Id == id);
            if (index == -1)
            {
                return false;
            }
            Desk temporalDesk = desks[index];
            if (temporalDesk.BookedBy != "")
            {
                throw new ArgumentNullException("Desk is already booked by other user");
            }
            temporalDesk.Name = username;
            //BookingDate = DateTime.UtcNow, BookingExpirationDate = DateTime.UtcNow.AddDays(1)
            temporalDesk.BookingDate = DateTime.UtcNow;
            if(days>7||days<=0)
            {
                throw new ArgumentNullException("Book Desk For Multiple Days But Not More Than Week, too many days provided");
            }
            temporalDesk.BookingExpirationDate = DateTime.UtcNow.AddDays(days);
            desks.RemoveAt(index);
            desks.Add(temporalDesk);
            return true;
        }
        public bool ChangeDeskButNotLaterThanTwentyFourHoursAfterReservation(int id, string username)
            //najpierw musze sprawdzic czy desk o tym idku jest zajety, pozniej czy mam desk juz zajety jakis, a pozniej jezeli mam to zresetowac go i wziasc nowy
        {
            if (username == null || username == "")
            {
                throw new ArgumentNullException("Provided username is empty");
            }
            int index = desks.FindIndex(p => p.Id == id);
            if (index == -1)
            {
                return false;
            }
            Desk temporalDesk = desks[index];
            if (temporalDesk.BookedBy != "")
            {
                throw new ArgumentNullException("Desk is already booked by other user");
            }
            Desk potentiallyMyDesk=desks.Find(p => p.BookedBy.Equals(username));//pozniej czy mam desk juz zajety jakis
            Desk temporalDesk2 = potentiallyMyDesk;
            if (potentiallyMyDesk!=null)//if i already have desk
            {
                if((temporalDesk2.BookingDate - DateTime.UtcNow).TotalHours > 24)
                {
                    throw new ArgumentNullException("Can not change desk later than 24 hours after reservation.");
                }
                desks.Remove(temporalDesk2);
                desks.Add(new Desk {Name = temporalDesk2.Name, Location = temporalDesk2.Location, BookedBy = "", BookingDate = DateTime.UtcNow, BookingExpirationDate = DateTime.UtcNow.AddDays(1) });
                
            }
            else
            {
                throw new ArgumentNullException("You can't change desk if you didn't reserve one first.");
            }
            temporalDesk.BookedBy = username;
            //BookingDate = DateTime.UtcNow, BookingExpirationDate = DateTime.UtcNow.AddDays(1)
            temporalDesk.BookingDate = DateTime.UtcNow;
            temporalDesk.BookingExpirationDate = temporalDesk2.BookingExpirationDate;
            index = desks.FindIndex(p => p.Id == id);
            if (index == -1)
            {
                return false;
            }
            desks.RemoveAt(index);
            desks.Add(temporalDesk);
            return true;
        }
        

    }
}
