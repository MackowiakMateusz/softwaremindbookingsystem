﻿namespace SoftwareMindDeskBookingSystem.Models
{
    public interface ILocationRepository
    {
        //gets
        IEnumerable<Location> GetAll();
        Location Get(int id);
        //posts
        Location Add(Location desk);
        //deletes
        void Remove(int id);
        
    }
}
