﻿namespace SoftwareMindDeskBookingSystem.Models
{
    public class Desk
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Location { get; set; }
        public string BookedBy { get; set; }
        public DateTime BookingDate { get; set; }
        public DateTime BookingExpirationDate { get; set; }
    }
}
