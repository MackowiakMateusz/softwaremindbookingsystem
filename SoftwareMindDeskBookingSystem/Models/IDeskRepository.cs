﻿namespace SoftwareMindDeskBookingSystem.Models
{
    public interface IDeskRepository
    {
        //gets
        IEnumerable<Desk> GetAllAdmin();
        IEnumerable<Desk> GetAll();
        Desk Get(int id);
        //posts
        Desk Add(Desk desk);
        //deletes
        void Remove(int id);
        //puts
        bool Update(Desk desk);//will not be used propably
        bool BookDeskForOneDay(int id, string username);
        bool BookDeskForMultipleDaysButNotMoreThanWeek(int id,int days, string username);
        bool ChangeDeskButNotLaterThanTwentyFourHoursAfterReservation(int id, string username);//id of new desk, username of user that changes desk
        
    }
}
