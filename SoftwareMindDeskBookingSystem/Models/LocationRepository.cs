﻿namespace SoftwareMindDeskBookingSystem.Models
{
    public class LocationRepository : ILocationRepository
    {
        private List<Location> locations = new List<Location>();
        private int _nextId = 1;

        public LocationRepository()
        {
            Add(new Location { Name = "1stFloor"});
            Add(new Location { Name = "2ndFloor" });
            Add(new Location { Name = "3rdFloor" });
        }

        public IEnumerable<Location> GetAll()
        {
            return locations;
        }

        public Location Get(int id)
        {
            return locations.Find(p => p.Id == id);
        }

        public Location Add(Location item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("item");
            }
            item.Id = _nextId++;
            locations.Add(item);
            return item;
        }

        public void Remove(int id)
        {
            locations.RemoveAll(p => p.Id == id);
        }
        

    }
}
