﻿using Microsoft.AspNetCore.Mvc;
using SoftwareMindDeskBookingSystem.Models;
using SoftwareMindDeskBookingSystem.Auth;
using Microsoft.AspNetCore.Authorization;
namespace SoftwareMindDeskBookingSystem.Controllers
{
    
    [Route("api/[controller]")]
    [ApiController]
    public class DeskController : ControllerBase
    {
        static readonly IDeskRepository repository = new DeskRepository();
        //gets
        [Authorize(Roles = UserRoles.Admin)]
        [HttpGet]
        [Route("desks/getAllAdmin")]
        public IEnumerable<Desk> GetAllAdmin()
        {
            return repository.GetAllAdmin();
        }
        
        [HttpGet]
        [Route("desks/getAll")]
        public IEnumerable<Desk> GetAllDesks()
        {
            return repository.GetAll();
        }
        [HttpGet]
        [Route("desks/getAvaible")]
        public IEnumerable<Desk> GetAvaibleDesks()
        {
            return repository.GetAll().Where(
                p => string.Equals(p.BookedBy, "", StringComparison.OrdinalIgnoreCase));
        }
        [HttpGet]
        [Route("desks/getUnavaible")]
        public IEnumerable<Desk> GetUnavaibleDesks()
        {
            return repository.GetAll().Where(
                p => !string.Equals(p.BookedBy, "", StringComparison.OrdinalIgnoreCase));
        }
        
        [HttpGet]
        [Route("desks/getById")]
        public Desk GetDesk(int id)
        {
            Desk item = repository.Get(id);
            if (item == null)
            {
                throw new HttpRequestException();
            }
            return item;
        }
        [HttpGet]
        [Route("desks/getByLocation")]
        public IEnumerable<Desk> GetDesksByLocation(string location)
        {
            return repository.GetAll().Where(
                p => string.Equals(p.Location, location, StringComparison.OrdinalIgnoreCase));
        }
        //posts
        [Authorize(Roles = UserRoles.Admin)]
        [HttpPost]
        [Route("desks/addDesk")]
        public Desk PostDesk(Desk item)
        {
            item = repository.Add(item);
            return item;
        }
        //puts
        [HttpPut]
        [Route("desks/bookForOneDay")]
        public void BookDeskForOneDay(int id, string username)
        {
            if (!repository.BookDeskForOneDay(id,username))
            {
                throw new HttpRequestException();
            }
        }
        [HttpPut]
        [Route("desks/bookForMultipleDaysButNotMoreThanWeek")]
        public void BookDeskForMultipleDaysButNotMoreThanWeek(int id, int days, string username)
        {
            if (!repository.BookDeskForMultipleDaysButNotMoreThanWeek(id,days,username))
            {
                throw new HttpRequestException();
            }
        }
        [HttpPut]
        [Route("desks/changeDeskButNotLaterThanTwentyFourHoursAfterReservation")]
        public void ChangeDeskButNotLaterThanTwentyFourHoursAfterReservation(int id, string username)
        {
            if (!repository.ChangeDeskButNotLaterThanTwentyFourHoursAfterReservation(id,username))
            {
                throw new HttpRequestException();
            }
        }
        //deletes
        [Authorize(Roles = UserRoles.Admin)]
        [HttpDelete]
        [Route("desks")]
        public void DeleteDesk(int id)
        {
            Desk item = repository.Get(id);
            if (item == null)
            {
                throw new HttpRequestException();
            }

            repository.Remove(id);
        }
    }
}
