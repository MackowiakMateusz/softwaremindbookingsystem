﻿using Microsoft.AspNetCore.Mvc;

namespace SoftwareMindDeskBookingSystem.Controllers
{
    public class HomeController : Controller
    {//to mozna wywalic bo frontend zrobiony w reakcie
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        //public IActionResult Index()
        //{
        //    return View();
        //}
        public IActionResult Register()
        {
            return View();
        }
        public IActionResult Login()
        {
            return View();
        }
    }
}
