﻿using Microsoft.AspNetCore.Mvc;
using SoftwareMindDeskBookingSystem.Models;
using SoftwareMindDeskBookingSystem.Auth;
using Microsoft.AspNetCore.Authorization;
namespace SoftwareMindDeskBookingSystem.Controllers
{
    [Authorize(Roles =UserRoles.Admin)]
    [Route("api/[controller]")]
    [ApiController]
    public class LocationController : ControllerBase
    {
        static readonly IDeskRepository deskRepository = new DeskRepository();
        static readonly ILocationRepository repository = new LocationRepository();
        //gets
        [HttpGet]
        [Route("locations/getAll")]
        public IEnumerable<Location> GetAllLocations()
        {
            return repository.GetAll();
        }
        [HttpGet]
        [Route("locations/getLocationsWithoutBookings")]
        public IEnumerable<Location> GetLocationsWithoutBookings()
        {
            return repository.GetAll().Where(
                 p => int.Equals(0, deskRepository.GetAll().Where(
                 pp => string.Equals(pp.Location, p.Name, StringComparison.OrdinalIgnoreCase)).Count()));
        }
        [HttpGet]
        [Route("locations/getLocationsWithBookings")]
        public IEnumerable<Location> GetLocationsWithBookings()
        {
            return repository.GetAll().Where(
                p => !int.Equals(0, deskRepository.GetAll().Where(
                pp => string.Equals(pp.Location, p.Name, StringComparison.OrdinalIgnoreCase)).Count()));
        }
        
        [HttpGet]
        [Route("locations/getById")]
        public Location GetLocation(int id)
        {
            Location item = repository.Get(id);
            if (item == null)
            {
                throw new HttpRequestException();
            }
            return item;
        }
        
        //posts
        [HttpPost]
        [Route("locations/addLocation")]
        public Location PostLocation(Location item)
        {
            item = repository.Add(item);
            return item;
        }
       
        //deletes
        [HttpDelete]
        [Route("locations")]
        public void DeleteLocation(int id)
        {
            if(deskRepository.GetAll().Where(
                pp => string.Equals(GetLocation(id).Name, pp.Location, StringComparison.OrdinalIgnoreCase)).Count() > 0)
            {
                throw new HttpRequestException("Location has bookings, can't remove");
            }
            Location item = repository.Get(id);
            if (item == null)
            {
                throw new HttpRequestException();
            }

            repository.Remove(id);
        }
    }
}
